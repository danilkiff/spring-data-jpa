package ru.example.divr.spring.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@Entity
@Table(name = "users", uniqueConstraints = {
    @UniqueConstraint(columnNames = "userID"),
    @UniqueConstraint(columnNames = "login")
})
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userID", unique = true, nullable = false)
    private Integer userID;

    @Column(name = "login", unique = true, nullable = false)
    private String login;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
        name = "users2user_roles",
        joinColumns = {
            @JoinColumn(name = "userID", nullable = false, updatable = false)
        },
        inverseJoinColumns = {
            @JoinColumn(name = "roleID", nullable = false, updatable = false)
        }
    )
    private Set<Role> roles;

    public User(String login, Set<Role> roles) {
        this.login = login;
        this.roles = roles;
    }

    public User() {

    }
}
