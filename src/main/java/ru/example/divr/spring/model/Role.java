package ru.example.divr.spring.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_roles", uniqueConstraints = {
    @UniqueConstraint(columnNames = "roleID"),
    @UniqueConstraint(columnNames = "role")
})
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "roleID", unique = true, nullable = false)
    private Integer roleID;

    @Column(name = "role", unique = true, nullable = false)
    private String role;

    @Column(name = "description", nullable = true)
    private String description;

    public Role(String role, String description) {
        this.role = role;
        this.description = description;
    }

    public Role() {

    }
}
