package ru.example.divr.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.example.divr.spring.model.User;
import ru.example.divr.spring.repository.UserRepository;

import java.util.List;

@RestController
public class UserController implements Restful<User, Integer> {
    @Autowired
    private UserRepository repository;

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> findOne(@PathVariable("id") Integer id) {
        final User body = repository.findOne(id);
        final HttpStatus status = (body != null) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(body, status);
    }

    @RequestMapping(value = "/users/{page}/{size}", method = RequestMethod.GET)
    public ResponseEntity<Page<User>> findPage(@PathVariable("page") int page, @PathVariable("to") int size) {
        final Page<User> body = repository.findAll(new PageRequest(page, size));
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ResponseEntity<List<User>> findAll() {
        final List<User> body = repository.findAll();
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity<User> insert(@RequestBody User role) {
        final User body = repository.saveAndFlush(role);
        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> update(@PathVariable("id") Integer id, @RequestBody User role) {
        final boolean found = repository.exists(id);
        final HttpStatus status = found ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        final User body = found ? repository.saveAndFlush(role) : null;
        return new ResponseEntity<>(body, status);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id) {
        if (repository.exists(id)) {
            repository.delete(id);
            repository.flush();
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
