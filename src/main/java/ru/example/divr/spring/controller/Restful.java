package ru.example.divr.spring.controller;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface Restful<EntityType, PrimaryKeyType> {
    ResponseEntity<EntityType> insert(EntityType entity);

    ResponseEntity<EntityType> findOne(PrimaryKeyType id);
    ResponseEntity<Page<EntityType>> findPage(int page, int size);
    ResponseEntity<List<EntityType>> findAll();

    ResponseEntity<EntityType> update(PrimaryKeyType id, EntityType entity);
    ResponseEntity<Void> delete(PrimaryKeyType id);
}
