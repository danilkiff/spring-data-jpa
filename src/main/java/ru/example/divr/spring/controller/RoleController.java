package ru.example.divr.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.example.divr.spring.repository.RoleRepository;
import ru.example.divr.spring.model.Role;

import java.util.List;

@RestController
public class RoleController implements Restful<Role, Integer> {
    @Autowired
    private RoleRepository repository;

    @RequestMapping(value = "/userRoles/{id}", method = RequestMethod.GET)
    public ResponseEntity<Role> findOne(@PathVariable("id") Integer id) {
        final Role body = repository.findOne(id);
        final HttpStatus status = (body != null) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(body, status);
    }

    @RequestMapping(value = "/userRoles/{page}/{size}", method = RequestMethod.GET)
    public ResponseEntity<Page<Role>> findPage(@PathVariable("page") int page, @PathVariable("to") int size) {
        final Page<Role> body = repository.findAll(new PageRequest(page, size));
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @RequestMapping(value = "/userRoles", method = RequestMethod.GET)
    public ResponseEntity<List<Role>> findAll() {
        final List<Role> body = repository.findAll();
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @RequestMapping(value = "/userRoles", method = RequestMethod.POST)
    public ResponseEntity<Role> insert(@RequestBody Role role) {
        final Role body = repository.saveAndFlush(role);
        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/userRoles/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Role> update(@PathVariable("id") Integer id, @RequestBody Role role) {
        final boolean found = repository.exists(id);
        final HttpStatus status = found ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        final Role body = found ? repository.saveAndFlush(role) : null;
        return new ResponseEntity<>(body, status);
    }

    @RequestMapping(value = "/userRoles/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id) {
        if (repository.exists(id)) {
            repository.delete(id);
            repository.flush();
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
