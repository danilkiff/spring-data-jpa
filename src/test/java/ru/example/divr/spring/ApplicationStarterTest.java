package ru.example.divr.spring;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Set;

@TestPropertySource(properties = {
    "spring.jmx.enabled:true",
    "spring.datasource.jmx-enabled:true"
})
public class ApplicationStarterTest extends AbstractUnitTest {
    private MBeanServer mBeanServer;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        mBeanServer = ManagementFactory.getPlatformMBeanServer();
    }

    @Test
    public void testHomeIsFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/"))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testUsersEndpoint() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/users"))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testRolesEndpoint() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/userRoles"))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testJmxConnectionPoolExists() throws Exception {
        final ObjectName name = new ObjectName("jpa.test:type=ConnectionPool,*");
        final Set<ObjectInstance> set = mBeanServer.queryMBeans(name, null);
        Assertions.assertThat(set).hasSize(1);
    }
}
