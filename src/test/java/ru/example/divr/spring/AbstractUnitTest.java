package ru.example.divr.spring;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class AbstractUnitTest {
    @Autowired
    protected WebApplicationContext context;

    @Autowired
    protected ObjectMapper mapper;

    protected MockMvc mvc;

    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    };

    public String toJson(final Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }
}
