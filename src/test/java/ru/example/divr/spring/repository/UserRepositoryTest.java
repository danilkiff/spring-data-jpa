package ru.example.divr.spring.repository;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.example.divr.spring.AbstractUnitTest;
import ru.example.divr.spring.model.User;

import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserRepositoryTest extends AbstractUnitTest {

    @Before
    @Override
    public void setUp() {
        super.setUp(); // @Before не наследуется, бугага
    }

    @Test
    public void test01_CreateSingleUser() throws Exception {
        final User user = new User("user", Collections.emptySet());
        final RequestBuilder request = MockMvcRequestBuilders.post("/users")
            .content(toJson(user))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON);
        mvc.perform(request)
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andReturn();
    }

    @Test
    public void test02_GetSingleUser() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/users/{id}", 1)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test03_UpdateSingleUser() throws Exception {
        final User user = new User("user", Collections.emptySet());
        user.setUserID(1);
        user.setLogin("renamed");

        final RequestBuilder request = MockMvcRequestBuilders.put("/users/{id}", 1)
            .content(toJson(user))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON);
        mvc.perform(request)
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.login", equalTo(user.getLogin())))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test04_DeleteSingleUser() throws Exception {
        final RequestBuilder request = MockMvcRequestBuilders.delete("/users/{id}", 1)
            .accept(MediaType.APPLICATION_JSON);
        mvc.perform(request)
            .andExpect(MockMvcResultMatchers.status().isNoContent())
            .andDo(MockMvcResultHandlers.print());
    }
}
